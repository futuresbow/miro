
<form method="post">


 <p><strong>Mi a kérdés, amit Te válaszolsz meg először?</strong></p>
<div class="input-group mb-3">
	
 
	
  <div class="input-group-prepend">
     <select class="custom-select" id="inputGroupSelect02">
		<option selected>Miről szól a(z)</option>
		<option value="1">Mi az értelme a(z) </option>
		<option value="2">Mitől jó a(z) </option>
		<option value="3">Mit jelent a(z) </option>
	  </select>
	 
  </div>
  <input type="text" name="a[cim]" placeholder="Írd ide a kérdést" class="form-control" aria-label="Text input with dropdown button">
</div>

<div class="form-group">
    <label for="cikk">A Te válaszod <small>- figyelmesen olvasd át, ha végeztél, mert az első hozzászólás után már nem módosíthatod, amit írtál</small></label>
    <textarea  name="a[cikk]" class="form-control cikktext" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div>

<div class="form-group">
	<label>Milyen témákba sorolnád az írásodat?</label>
	
	<div class="row">
		<div class="col-3">
			<select class="form-control" onchange="labelHozzaadas(this)" >
				<option value="0">Válassz cimkét</option>
				<?php foreach($this->Sql->gets("kerdeskategoriak", "ORDER BY bejegyzesszam ASC LIMIT 10") as $sor):?>
				<option value="<?= $sor->id;?>" class="opcio<?= $sor->id;?>"><?= $sor->nev; ?></option>
				<?php endforeach;?>
			</select>
		</div>
		<div class="col cimketar">
			Kiválasztott cimkék: 
		</div>
	</div>
	

</div>
<script>
$().ready(function(){ $('.selectpicker').selectpicker(); })
function labelHozzaadas(o) {
	opcio = o.options[o.selectedIndex];
	label = $(opcio).html();
	id = $(opcio).val();
	$('.opcio'+id).hide();
	
	$('.cimketar').append('<button type="button" class="btn btn-secondary cimkebtn" onclick="delthis(this)" data-id="'+id+'" name="cimke['+id+']">'+label+' X</button>');
	
}
function delthis(o) {
	id = $(o).attr('data-id');
	$('.opcio'+id).show();
	$(o).remove();
}
</script>




<?php if(!$tag):?>

<div class="belepoform">
	
	
	<div class="row">
		<div class="col-sm">
		  <a href="javascript:void(0);" onclick="$('#loginModal').modal();" class="btn btn-success  btn-block">Kattint ide, ha már van fiókod és be tudsz lépni...</a>
		</div>
		<div class="col-sm">
		  <a href="javascript:void(0);" onclick="$('#regModal').modal();" class="btn btn-info  btn-block">Új vagy itt, regisztrálj!</a>
		</div>
		
	</div>
	
	
 </div>
<?php else: ?>

<?php endif; ?>
<div class="regLeiras" style="display:none">
<p>A regisztrációd után aktivációs levelet küldtünk Neked. Kérlek, kattints az aktiváló linkre a levélben. Így tudod biztosítani, hogy írásod folyamatosan olvasható legyen az oldalon.</p>
</div>

<div class="custom-file hidden filefeltolto">
  <input name="kep" type="file" class="custom-file-input" id="customFile">
  <label  class="custom-file-label" for="customFile">Kép feltöltése (ügyelj a méretre, maximum 2 MB, 1000px * 800px)</label>
</div>
<p><br /></p>
<button type="button" onclick="$('.filefeltolto').toggleClass('hidden');" class="btn btn-secondary btn-sm kuldesGomb float-right" style="<?php if(!$tag):?>display:none;<?php endif;?>">További lehetőségek</button>
<button type="submit" class="btn btn-barna kuldesGomb" style="<?php if(!$tag):?>display:none;<?php endif;?>">Közzéteszem az írásomat</button>


</form>
<br><br><br><br><br><br><br>
