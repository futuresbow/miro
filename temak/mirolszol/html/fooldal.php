
  <!-- Main jumbotron for a primary marketing message or call to action -->
  
	
	
	<div class="container">
    <!-- Example row of columns -->
    <div class="row">
      <div class="col-md-3">
        <h4 class="text-secondary">Legjobb kérdések</h4>
        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
        <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
      </div>
      <div class="col-md-3">
        <h4 class="text-secondary">Legsikeresebb válaszok</h4>
        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
        <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
      </div>
      <div class="col-md-3">
        <h4 class="text-secondary">Még nincs válasz</h4>
        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
        <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
      </div>
      <div class="col-md-3 text-center">
        
        <h4 class="text-secondary">Van egy jó témád?</h4>
        <p>&nbsp;</p>
        
        <p>
			<a href="<?= base_url();?>cikkiras" class="btn btn-barna btn-lg btn-block">Írj egy cikket</a>
        </p>
        
        
        
        
      </div>
    </div>

    <hr>
	
