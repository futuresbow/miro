-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 01, 2020 at 11:34 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mirolszolcom`
--

-- --------------------------------------------------------

--
-- Table structure for table `adminmenu`
--

CREATE TABLE `adminmenu` (
  `id` int(11) NOT NULL,
  `szulo_id` int(11) NOT NULL DEFAULT 0,
  `modul_eleres` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `felirat` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(11) NOT NULL DEFAULT 0,
  `ikonosztaly` varchar(100) COLLATE utf8_hungarian_ci NOT NULL DEFAULT '',
  `jogkor` int(11) NOT NULL DEFAULT 63
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `adminmenu`
--

INSERT INTO `adminmenu` (`id`, `szulo_id`, `modul_eleres`, `felirat`, `sorrend`, `ikonosztaly`, `jogkor`) VALUES
(1, 0, '', 'Vezérlőpult', 0, 'dashboard', 63),
(2, 0, '', 'Rendelések', 90, 'orders', 63),
(3, 0, '', 'Termékek', 110, 'products', 63),
(4, 0, '', 'Felhasználók', 260, 'users', 63),
(5, 0, '', 'Tartalomkezelés', 290, 'cms', 63),
(6, 0, '', 'Beállítások', 460, 'settings', 63),
(7, 3, 'termek/lista', 'Összes termék', 120, '', 63),
(8, 3, 'termek/szerkesztes/0', 'Új termék', 130, '', 63),
(9, 3, 'elvalaszto', '', 140, '', 63),
(10, 3, 'kategoria/lista', 'Összes kategória', 170, '', 63),
(11, 3, 'kategoria/szerkesztes/0', 'Új kategória', 180, '', 63),
(12, 6, 'beallitasok/altalanos', 'Általános beállítások', 470, '', 63),
(13, 2, 'rendelesek/lista', 'Összes megrendelés', 100, '', 63),
(14, 5, 'beallitasok/adminmenulista', 'Admin menüpontok', 310, '', 32),
(15, 6, 'fizetesmodok/lista', 'Fizetési módok', 490, '', 63),
(16, 6, 'fizetesmodok/szerkesztes/0', 'Új fizetési mód', 500, '', 63),
(17, 6, 'elvalaszto', '', 480, '', 63),
(18, 6, 'szallitasmodok/lista', 'Szállítási módok', 520, '', 63),
(19, 6, 'szallitasmodok/szerkesztes/0', 'Új szállítási mód', 530, '', 63),
(20, 6, 'elvalaszto', '', 510, '', 63),
(22, 5, 'elvalaszto', '', 320, '', 63),
(23, 3, 'elvalaszto', '', 200, '', 63),
(24, 3, 'afaespenznem/lista', 'Áfa értékek', 210, '', 63),
(25, 3, 'afaespenznem/penznemlista', 'Pénznemek', 230, '', 63),
(26, 3, 'elvalaszto', '', 220, '', 63),
(27, 3, 'elvalaszto', '', 240, '', 63),
(28, 3, 'gyartok/lista', 'Gyártók', 250, '', 63),
(29, 4, 'felhasznalok/lista', 'Felhasználók', 270, '', 63),
(30, 4, 'felhasznalok/vasarlolista', 'Vásárlók', 280, '', 32),
(31, 5, 'hirlevel/sablonlista', 'E-mail sablonok', 370, '', 63),
(32, 5, 'hirlevel/temaszerkesztes', 'E-mail kinézet', 380, '', 63),
(33, 5, 'elvalaszto', '', 360, '', 63),
(34, 5, 'hirlevel/hirlevellista', 'Hírlevél létrehozás', 390, '', 63),
(35, 5, 'hirlevel/cron', 'Hírlevél cron teszt', 400, '', 32),
(36, 5, 'slider/lista', 'Sliderek', 420, '', 63),
(37, 5, 'elvalaszto', '', 410, '', 63),
(38, 5, 'post/lista', 'Bejegyzések', 440, '', 63),
(39, 5, 'post/kategorialista', 'Bejegyzés kategóriák', 450, '', 63),
(40, 5, 'elvalaszto', '', 430, '', 63),
(41, 5, 'beallitasok/menulista', 'Frontend menüpontok', 300, '', 63),
(42, 5, 'seo/lista', 'SEO tartalmak', 350, '', 63),
(43, 3, 'termek/keszletek', 'Termékkészletek', 150, '', 63),
(44, 3, 'elvalaszto', '', 160, '', 63),
(45, 3, 'termekcimkek/lista', 'Termék cimkék', 190, '', 63),
(46, 0, '', 'Oldalak', 50, '', 63),
(47, 46, 'oldalak/urllista', 'Elérhető URL-ek', 60, '', 63),
(48, 46, 'oldalak', 'dinamikus', 70, '', 63),
(49, 46, 'oldalak/tartalomepito_uj/1', 'Főoldal teszt szerkesztő', 80, '', 32),
(50, 0, '', 'Témák', 10, '', 63),
(51, 50, 'temak/temavalaszto', 'Témaválasztó', 20, '', 63),
(52, 50, 'temak', 'dinamikus', 40, '', 63),
(53, 5, 'import/index', 'Importálás', 330, '', 32),
(54, 5, 'elvalaszto', '', 340, '', 63),
(55, 50, 'elvalaszto', '', 30, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `aruhazak`
--

CREATE TABLE `aruhazak` (
  `id` int(11) NOT NULL,
  `nev` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `prefix` varchar(10) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `aruhazak`
--

INSERT INTO `aruhazak` (`id`, `nev`, `prefix`) VALUES
(5, 'Mirolszol.com', '');

-- --------------------------------------------------------

--
-- Table structure for table `cikkek`
--

CREATE TABLE `cikkek` (
  `id` int(11) NOT NULL,
  `cim` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `valasz` text COLLATE utf8_hungarian_ci NOT NULL,
  `megtekintesek` int(11) NOT NULL,
  `kedvelesek` int(11) NOT NULL,
  `szerzo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email_sablonok`
--

CREATE TABLE `email_sablonok` (
  `id` int(11) NOT NULL,
  `targy` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `html` text COLLATE utf8_hungarian_ci NOT NULL,
  `kulcs` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(3) COLLATE utf8_hungarian_ci NOT NULL DEFAULT 'hu'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `email_sablonok`
--

INSERT INTO `email_sablonok` (`id`, `targy`, `html`, `kulcs`, `nyelv`) VALUES
(1, 'Megrendelésedet sikeresen leadtad', 'Kedves {{Teljes név}},\r\n\r\nRendelésed rögzítettük. A rendelés adatai:\r\n{{Rendelés adatok}}\r\n\r\nKöszönjük, hogy minket választottál!\r\nÜdvözlettel:\r\nCPWebshop\r\n', 'rendeles_mentes_visszaigazolas', 'hu'),
(2, 'Hamarosan csomagod érkezik!', 'Kedves {{Teljes név}},\r\n\r\nÖrömmel értesítünk, hogy csomagod elkészült és várhatóan egy napon belül megérkezik. \r\n{{Csomagkövetés}}\r\nA rendelés adatai:\r\n{{Rendelés adatok}}\r\n\r\nKöszönjük, hogy minket választottál!\r\nÜdvözlettel:\r\nCPWebshop\r\n', 'rendeles_feladas_visszaigazolas', 'hu'),
(3, 'Kérjük, erősítsd meg regisztrációdat', 'Kedves {{Teljes név}},\r\n\r\nkérjük, kattints az <a href=\"{{Aktivációs link}}\">aktivációs linkre</a>, vagy nyisd meg böngésződben a következő linket:\r\n{{Aktivációs link}}\r\n\r\nKöszönjük, hogy minket választottál!\r\nÜdvözlettel:\r\nCPWebshop\r\n', 'regisztracios_megerosito', 'hu'),
(4, 'Üdvözlünk körünkben', 'Kedves {{Teljes név}},\r\n\r\nÜdvözlünk rendszerünkben. Nézd meg legújabb ajánlatainkat!\r\n\r\n\r\nKöszönjük, hogy minket választottál!\r\nÜdvözlettel:\r\nCPWebshop', 'regisztracio_visszaigazolas', 'hu'),
(6, 'Rendelésedet sikeresen teljesítettük!', '<p><b>Kedves {{$Teljes név}},</b></p>\r\n<p>\r\nrendelésed teljesítésre került. Reméljük, elégedett voltál a szolgáltatásunkkal. Korábbi rendeléseidet megnézheted oldalunkon: {{Oldal URL}}.</p>\r\n\r\n<p>Rendelés azonosítód: <i><b>{{Rendelés ID}}</b></i></p>', 'rendeles_visszaigazolo_kesz', 'hu'),
(7, 'Köszönjük megrendelését ', 'Kedves {{Teljes név}},\r\n\r\nÖn áruházunkban {{Rendelés ID}} azonosító számmal rendelést adott le. Köszönjük, áruházunk fogadta azt. Munkatársaink hamarosan feldolgozzák.\r\n\r\n\r\n', 'rendeles_visszaigazolo_leadas', 'hu'),
(8, 'Jelszó visszaállítási link', 'Üdvözlöm, \r\n\r\noldalunkon jelszó visszaállítást kértek a {{Email}} E-mail cím megadásával. Amennyiben nem Ön kérte a visszaállítást, kérjük, hagyja figyelmen kívül ezt a levelet.\r\nA visszaállításhoz kattintson a lenti linkre, vagy másolja be böngészőjébe:\r\n\r\n<a href=\"{{Link}}\">{{Link}}</a>\r\n\r\nÜdvözlettel:\r\nXY Webáruház', 'elfelejtett_jelszo_visszaallitas', 'hu');

-- --------------------------------------------------------

--
-- Table structure for table `felhasznalok`
--

CREATE TABLE `felhasznalok` (
  `id` int(11) NOT NULL,
  `regtipus` varchar(30) COLLATE utf8_hungarian_ci NOT NULL,
  `smedia_id` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `vezeteknev` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `keresztnev` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `adoszam` varchar(20) COLLATE utf8_hungarian_ci NOT NULL DEFAULT '',
  `jelszo` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `nick` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `regisztracio` timestamp NOT NULL DEFAULT current_timestamp(),
  `statusz` int(11) NOT NULL DEFAULT 0,
  `ellenorzokulcs` varchar(255) COLLATE utf8_hungarian_ci NOT NULL DEFAULT '',
  `adminjogok` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `felhasznalok`
--

INSERT INTO `felhasznalok` (`id`, `regtipus`, `smedia_id`, `vezeteknev`, `keresztnev`, `email`, `adoszam`, `jelszo`, `nick`, `regisztracio`, `statusz`, `ellenorzokulcs`, `adminjogok`) VALUES
(4, '', '', 'Ceglédi', 'Iván', 'futuresbow@gmail.com', '', '981e181b7444ec1cb939aafeb6a2b243', '', '2019-04-04 10:52:36', 0, '', 4),
(5, '', '', 'Ceglédi', 'Iván', 'cegledi.ivan74@gmail.com', '32132321321', '981e181b7444ec1cb939aafeb6a2b243', '', '2019-04-04 10:55:42', 0, '', 32),
(15, '', '', 'Tóth', 'László', 'laci@cpinfo.hu', '', '41905f54681c3722e888c9201d8e0d8d', '', '2019-05-17 10:56:57', 1, '', 32),
(16, '', '', 'Bíró', 'Marci', 'marci@cpinfo.hu', '', '6c1de5700ad15bf09d54cc2781939695', '', '2019-05-17 11:00:34', 1, '', 4),
(17, '', '', 'Siklósi', 'Ricsi', 'ricsi@cpinfo.hu', '', '0f28216de776f6baed17b1b2b688bf53', '', '2019-05-17 11:00:53', 1, '5da8350734dac', 4),
(18, '', '', 'Langer', 'Balázs', 'balazs@cpinfo.hu', '', '544d60fa2812297960173a6802402f5e', '', '2019-05-17 11:01:29', 1, '', 1),
(19, '', '', 'Besenyei', 'Kálmán', 'bkalman@cpinfo.hu', '', 'fdaa1faa914f8b11799f29fd98467c04', '', '2019-05-17 11:02:47', 1, '', 8),
(20, '', '', 'Szita', 'Attila', 'attilaszita@gmail.com', '', 'f3e36a7b18a4becb8b7d686ab860f694', '', '2019-05-17 11:03:08', 1, '', 4),
(21, '', '', 'Vevő', 'Egy', 'vevo1@zente.org', '4324432', '981e181b7444ec1cb939aafeb6a2b243', '', '2019-10-02 13:19:52', 0, '', 0),
(22, '', '', 'Vevő', 'Ketto', 'vevo2@zente.org', '4324432', '981e181b7444ec1cb939aafeb6a2b243', '', '2019-10-02 13:25:44', 0, '', 0),
(23, '', '', 'Teszt', 'User', 'teszt_user@cpinfo.hu', '', 'ca03ad46a6f953a785666f4b251d18c8', '', '2019-10-03 09:23:16', 0, '', 0),
(24, '', '', 'Biró', 'Marcell', 'birmar2@index.hu', '', '352fe80dbca018dd9c3d4683ccad28c2', '', '2019-10-07 08:33:15', 0, '', 0),
(25, '', '', 'Teszt', 'Ember', 'cica@zente.org', '', '981e181b7444ec1cb939aafeb6a2b243', '', '2019-10-08 06:26:05', 0, '', 0),
(26, '', '', 'Teszt', 'Ricsi', 'ricsi2@cpinfo.hu', '', 'c3e017c29d814ab8c72b2cc68ee8a7fa', '', '2019-10-10 06:51:21', 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `felhasznalo_csoportok`
--

CREATE TABLE `felhasznalo_csoportok` (
  `id` int(11) NOT NULL,
  `nev` varchar(100) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `frontendmenu`
--

CREATE TABLE `frontendmenu` (
  `id` int(11) NOT NULL,
  `szulo_id` int(11) NOT NULL DEFAULT 0,
  `url` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `felirat` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(11) NOT NULL DEFAULT 0,
  `statusz` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `frontendmenu`
--

INSERT INTO `frontendmenu` (`id`, `szulo_id`, `url`, `felirat`, `sorrend`, `statusz`) VALUES
(1, 0, '', 'Főoldal', 0, 1),
(6, 0, 'kapcsolat', 'Kapcsolat', 20, 1),
(7, 0, 'hirlevel-feliratkozas', 'Hírlevél', 10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `galeriak`
--

CREATE TABLE `galeriak` (
  `id` int(11) NOT NULL,
  `nev` varchar(255) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `galeriak`
--

INSERT INTO `galeriak` (`id`, `nev`) VALUES
(1, 'Főoldali slider'),
(3, 'Szar2');

-- --------------------------------------------------------

--
-- Table structure for table `galeria_kepek`
--

CREATE TABLE `galeria_kepek` (
  `id` int(11) NOT NULL,
  `galeria_id` int(11) NOT NULL,
  `kep` varchar(200) COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `galeria_kepek`
--

INSERT INTO `galeria_kepek` (`id`, `galeria_id`, `kep`, `leiras`, `sorrend`) VALUES
(8, 1, 'assets/slider/1_park_canada_garden_pond_victoria_butchart_nature_81124_1920x1080.jpg', 'Leírás 1', 10),
(16, 1, 'assets/slider/1_main-slider-1180x400.jpg', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `hirlevelek`
--

CREATE TABLE `hirlevelek` (
  `id` int(11) NOT NULL,
  `targy` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `tartalom` text COLLATE utf8_hungarian_ci NOT NULL,
  `termeklista` text COLLATE utf8_hungarian_ci NOT NULL,
  `ido` timestamp NOT NULL DEFAULT current_timestamp(),
  `idozitve` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `kikuldott_db` int(11) NOT NULL DEFAULT 0,
  `statusz` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0:előkészítés alatt, 1: kiküldésre vár, 2: kiküldve, 4: törölve'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `hirlevelek`
--

INSERT INTO `hirlevelek` (`id`, `targy`, `tartalom`, `termeklista`, `ido`, `idozitve`, `kikuldott_db`, `statusz`) VALUES
(1, '{{ Keresztnév }}, ne maradj le a  LEGNAGYOBB TAVASZI AKCIÓKRÓL', '<p>Szia {{ Keresztnév }},</p><h2><span style=\"color: rgb(102, 102, 102);\">Elképesztő tavaszi akciók a CPWebshop termékeiből!</span> </h2><p></p><p>Válogass kedvedre a 30, 40, 50 %-os akcióink közül!!</p><p><span style=\"color: rgb(255, 255, 255);\"></span></p>', '3, 1, 2, ', '2019-03-20 11:32:50', '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `hirlevel_feliratkozok`
--

CREATE TABLE `hirlevel_feliratkozok` (
  `id` int(11) NOT NULL,
  `nev` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `regisztracio` timestamp NOT NULL DEFAULT current_timestamp(),
  `statusz` int(11) NOT NULL DEFAULT 0,
  `ellenorzokulcs` varchar(255) COLLATE utf8_hungarian_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `hirlevel_feliratkozok`
--

INSERT INTO `hirlevel_feliratkozok` (`id`, `nev`, `email`, `regisztracio`, `statusz`, `ellenorzokulcs`) VALUES
(1, 'Szuper Admin ', 'cegledi.ivan74@gmail.com', '2019-04-03 09:15:56', 0, 'd2bb0b77eab8c902e500a33cc13b78d9'),
(5, 'Ceglédi Iván', 'futuresbow@gmail.com', '2019-04-03 11:42:57', 0, 'e14f7e745792765b1b056da1d53fe2de'),
(6, 'Cegledi Ivan', 'ivan@zente.org', '2019-04-04 07:27:44', 0, '9187f1d99911dece4776da157af357a6'),
(7, 'Teszt User', 'teszt_user@cpinfo.hu', '2019-10-03 09:23:16', 0, 'c3893d58ce77ffa17c72ceb412b82516'),
(8, 'Biró Marcell', 'birmar2@index.hu', '2019-10-07 08:33:15', 0, 'bfc259407e1f48967180e449af6bf2ba'),
(9, 'Teszt Ember', 'cica@zente.org', '2019-10-08 06:26:05', 0, '056789f97f3c64ef0d0fc8b35c0f7ba2'),
(10, 'Teszt Ricsi', 'ricsi@cpinfo.hu', '2019-10-09 13:32:37', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `hozzaferesek`
--

CREATE TABLE `hozzaferesek` (
  `id` int(11) NOT NULL,
  `eleres` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `hatokor` enum('frontend','admin') COLLATE utf8_hungarian_ci NOT NULL DEFAULT 'frontend'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `hozzaferesek`
--

INSERT INTO `hozzaferesek` (`id`, `eleres`, `hatokor`) VALUES
(1, 'frontendlapok/frontendlapok/fooldalitartalmak', 'frontend'),
(2, 'oldalak/oldalak_admin/urllista', 'admin'),
(3, 'oldalak/oldalak_admin/urlszerkesztes', 'admin'),
(4, 'oldalak/oldalak_admin/tartalomepito', 'admin'),
(5, 'temak/temak_admin/beallitasok', 'admin'),
(6, 'rendelesek/rendelesek_admin/lista', 'admin'),
(7, 'rendelesek/rendelesek_admin/rendelesletrehozas', 'admin'),
(8, 'termek/termek_admin/lista', 'admin'),
(9, 'termek/termek_admin/szerkesztes', 'admin'),
(10, 'termek/termek_admin/keplista', 'admin'),
(11, 'termek/termek_admin/valtozatesopcio', 'admin'),
(12, 'termek/termek_admin/klonozas', 'admin'),
(13, 'termek/termek_admin/keszletek', 'admin'),
(14, 'termek/termek_admin/keszletszerkesztes', 'admin'),
(15, 'kategoria/kategoria_admin/lista', 'admin'),
(16, 'kategoria/kategoria_admin/szerkesztes', 'admin'),
(17, 'termekcimkek/termekcimkek_admin/lista', 'admin'),
(18, 'termekcimkek/termekcimkek_admin/szerkesztes', 'admin'),
(19, 'afaespenznem/afaespenznem_admin/lista', 'admin'),
(20, 'afaespenznem/afaespenznem_admin/szerkesztes', 'admin'),
(21, 'afaespenznem/afaespenznem_admin/penznemlista', 'admin'),
(22, 'afaespenznem/afaespenznem_admin/penznemszerkesztes', 'admin'),
(23, 'gyartok/gyartok_admin/lista', 'admin'),
(24, 'gyartok/gyartok_admin/szerkesztes', 'admin'),
(25, 'felhasznalok/felhasznalok_admin/lista', 'admin'),
(26, 'felhasznalok/felhasznalok_admin/szerkesztes', 'admin'),
(27, 'felhasznalok/felhasznalok_admin/vasarlolista', 'admin'),
(28, 'beallitasok/beallitasok_admin/menulista', 'admin'),
(29, 'beallitasok/beallitasok_admin/menuszerkesztes', 'admin'),
(30, 'beallitasok/beallitasok_admin/adminmenulista', 'admin'),
(31, 'beallitasok/beallitasok_admin/adminmenuszerkesztes', 'admin'),
(32, 'kinezetielemek/kinezetielemek_admin/lista', 'admin'),
(33, 'kinezetielemek/kinezetielemek_admin/szerkesztes', 'admin'),
(34, 'oldalak/oldalak_admin/tartalomepito_uj', 'admin'),
(35, 'seo/seo_admin/lista', 'admin'),
(36, 'seo/seo_admin/szerkesztes', 'admin'),
(37, 'hirlevel/hirlevel_admin/sablonlista', 'admin'),
(38, 'hirlevel/hirlevel_admin/sablonszerkesztes', 'admin'),
(39, 'hirlevel/hirlevel_admin/temaszerkesztes', 'admin'),
(40, 'hirlevel/hirlevel_admin/hirlevellista', 'admin'),
(41, 'hirlevel/hirlevel_admin/hirlevelszerkesztes', 'admin'),
(42, 'slider/slider_admin/lista', 'admin'),
(43, 'slider/slider_admin/szerkesztes', 'admin'),
(44, 'post/post_admin/lista', 'admin'),
(45, 'post/post_admin/szerkesztes', 'admin'),
(46, 'post/post_admin/kategorialista', 'admin'),
(47, 'post/post_admin/katszerkesztes', 'admin'),
(48, 'beallitasok/beallitasok_admin/altalanos', 'admin'),
(49, 'fizetesmodok/fizetesmodok_admin/lista', 'admin'),
(50, 'fizetesmodok/fizetesmodok_admin/szerkesztes', 'admin'),
(51, 'szallitasmodok/szallitasmodok_admin/lista', 'admin'),
(52, 'szallitasmodok/szallitasmodok_admin/szerkesztes', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `hozzaferesxcsoport`
--

CREATE TABLE `hozzaferesxcsoport` (
  `id` int(11) NOT NULL,
  `hozzaferesid` int(11) NOT NULL,
  `csoportid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kepek`
--

CREATE TABLE `kepek` (
  `id` int(11) NOT NULL,
  `filenev` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `szerep` enum('','főkép') COLLATE utf8_hungarian_ci NOT NULL DEFAULT '',
  `termek_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kerdeskategoriak`
--

CREATE TABLE `kerdeskategoriak` (
  `id` int(11) NOT NULL,
  `nev` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `bejegyzesszam` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `kerdeskategoriak`
--

INSERT INTO `kerdeskategoriak` (`id`, `nev`, `slug`, `bejegyzesszam`) VALUES
(1, 'Filmek', 'film', 20),
(2, 'Vers', 'vers', 2),
(3, 'Játékok', 'jatekok', 13),
(4, 'Könyvek', 'konyvek', 5);

-- --------------------------------------------------------

--
-- Table structure for table `kinezetielemek`
--

CREATE TABLE `kinezetielemek` (
  `id` int(11) NOT NULL,
  `nev` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `html` text COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `kinezetielemek`
--

INSERT INTO `kinezetielemek` (`id`, `nev`, `html`) VALUES
(1, 'Főoldali kéthasábos szerkezet nyitó elem', '<div class=\"homepage\">\r\n'),
(2, 'Bal oldali szűrőkonténer eleje', '<div class=\"left-side\">'),
(3, 'Bal oldali szűrőkonténer vége', '</div>'),
(4, 'Főoldali kéthasábos szerkezet záró elem', '</div>'),
(5, 'Jobb oldali tartalom hasáb eleje', '<div class=\"right-side\">\r\n'),
(6, 'Jobb oldali tartalom hasáb vége', '</div>');

-- --------------------------------------------------------

--
-- Table structure for table `leiratkozasok`
--

CREATE TABLE `leiratkozasok` (
  `id` int(11) NOT NULL,
  `kod` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `ido` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `leiratkozasok`
--

INSERT INTO `leiratkozasok` (`id`, `kod`, `ido`) VALUES
(1, '0a185f91cc0878bb7719b943213adeb4', '2019-04-03 11:29:20'),
(2, '0a185f91cc0878bb7719b943213adeb4', '2019-04-03 11:41:28'),
(3, '0a185f91cc0878bb7719b943213adeb4', '2019-04-03 11:42:40');

-- --------------------------------------------------------

--
-- Table structure for table `metaadatok`
--

CREATE TABLE `metaadatok` (
  `id` int(11) NOT NULL,
  `kulcs` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `ertek` text COLLATE utf8_hungarian_ci NOT NULL,
  `termek_id` int(11) NOT NULL DEFAULT 0,
  `sorrend` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `naplo`
--

CREATE TABLE `naplo` (
  `id` int(11) NOT NULL,
  `csoport` varchar(100) COLLATE utf8_hungarian_ci NOT NULL DEFAULT '',
  `bejegyzes` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `ido` timestamp NOT NULL DEFAULT current_timestamp(),
  `felhasznalo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `naplo`
--

INSERT INTO `naplo` (`id`, `csoport`, `bejegyzes`, `ido`, `felhasznalo_id`) VALUES
(1, 'felhasznalo', 'Marci Marci belépett az oldalra', '2019-05-17 11:21:36', 0),
(2, 'felhasznalo', 'Besenyei Kálmán belépett az oldalra', '2019-05-17 11:24:18', 0),
(3, 'felhasznalo', 'Besenyei Kálmán kilépett.', '2019-05-17 11:27:59', 0),
(4, 'felhasznalo', 'Cegledi Ivan belépett az oldalra', '2019-05-17 11:28:18', 0),
(5, 'felhasznalo', 'Cegledi Ivan az adminról kilépett.', '2019-05-17 11:30:18', 0),
(6, 'felhasznalo', 'Cegledi Ivan belépett az oldalra', '2019-05-17 11:55:44', 0),
(7, 'felhasznalo', 'Cegledi Ivan az adminról kilépett.', '2019-05-17 11:57:59', 0),
(8, 'felhasznalo', 'Ricsi Ricsi belépett az oldalra', '2019-05-17 11:58:24', 0),
(9, 'felhasznalo', 'Ricsi Ricsi kilépett.', '2019-05-17 11:58:27', 0),
(10, 'felhasznalo', 'Ceglédi Iván az adminról kilépett.', '2019-05-17 15:40:08', 0),
(11, 'felhasznalo', 'Tóth László belépett az oldalra', '2019-05-17 15:40:41', 0),
(12, 'felhasznalo', 'Tóth László az adminról kilépett.', '2019-05-17 15:47:39', 0),
(13, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-05-18 14:44:55', 0),
(14, 'felhasznalo', 'Ceglédi Iván kilépett.', '2019-05-18 14:45:33', 0),
(15, 'felhasznalo', 'Tóth László belépett az oldalra', '2019-05-19 20:00:07', 0),
(16, 'felhasznalo', 'Tóth László az adminról kilépett.', '2019-05-19 20:00:35', 0),
(17, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-05-20 06:46:03', 0),
(18, 'felhasznalo', 'Ceglédi Iván az adminról kilépett.', '2019-05-20 07:04:34', 0),
(19, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-05-20 07:05:01', 0),
(20, 'felhasznalo', 'Ceglédi Iván az adminról kilépett.', '2019-05-20 07:05:05', 0),
(21, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-05-20 07:07:16', 0),
(22, 'felhasznalo', 'Ceglédi Iván az adminról kilépett.', '2019-05-20 07:07:22', 0),
(23, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-05-20 07:11:56', 0),
(24, 'felhasznalo', 'Ceglédi Iván az adminról kilépett.', '2019-05-20 07:12:18', 0),
(25, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-05-20 07:12:20', 0),
(26, 'felhasznalo', 'Ceglédi Iván az adminról kilépett.', '2019-05-20 07:12:32', 0),
(27, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-05-20 07:13:01', 0),
(28, 'felhasznalo', 'Ceglédi Iván az adminról kilépett.', '2019-05-20 07:13:04', 0),
(29, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-05-20 07:17:11', 0),
(30, 'felhasznalo', 'Ceglédi Iván az adminról kilépett.', '2019-05-20 07:17:14', 0),
(31, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-05-20 07:17:17', 0),
(32, 'felhasznalo', 'Ceglédi Iván az adminról kilépett.', '2019-05-20 07:17:25', 0),
(33, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-05-20 07:19:19', 0),
(34, 'felhasznalo', 'Ceglédi Iván az adminról kilépett.', '2019-05-20 07:19:22', 0),
(35, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-05-20 08:12:27', 0),
(36, 'felhasznalo', 'Ceglédi Iván az adminról kilépett.', '2019-05-20 08:12:32', 0),
(37, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-05-20 08:12:38', 0),
(38, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-05-20 09:24:28', 0),
(39, 'felhasznalo', 'Ceglédi Iván az adminról kilépett.', '2019-05-20 10:09:30', 0),
(40, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-05-20 10:27:40', 0),
(41, 'felhasznalo', 'Balázs Balázs belépett az oldalra', '2019-05-20 11:09:05', 0),
(42, 'felhasznalo', 'Balázs Balázs az adminról kilépett.', '2019-05-20 11:09:10', 0),
(43, 'felhasznalo', 'Besenyei Kálmán belépett az oldalra', '2019-05-20 11:10:28', 0),
(44, 'felhasznalo', 'Besenyei Kálmán az adminról kilépett.', '2019-05-20 11:11:23', 0),
(45, 'felhasznalo', 'Langer Balázs belépett az oldalra', '2019-05-20 11:21:22', 0),
(46, 'felhasznalo', 'Langer Balázs az adminról kilépett.', '2019-05-20 11:21:28', 0),
(47, 'felhasznalo', 'Langer Balázs belépett az oldalra', '2019-05-20 11:21:42', 0),
(48, 'felhasznalo', 'Besenyei Kálmán belépett az oldalra', '2019-05-20 11:21:58', 0),
(49, 'felhasznalo', 'Besenyei Kálmán az adminról kilépett.', '2019-05-20 11:22:02', 0),
(50, 'felhasznalo', 'Tóth László belépett az oldalra', '2019-05-20 11:22:19', 0),
(51, 'felhasznalo', 'Besenyei Kálmán belépett az oldalra', '2019-05-20 11:51:26', 0),
(52, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-05-21 07:52:03', 0),
(53, 'felhasznalo', 'Langer Balázs belépett az oldalra', '2019-05-22 09:16:52', 0),
(54, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-05-23 07:26:08', 0),
(55, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-05-24 08:59:07', 0),
(56, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-05-24 11:24:41', 0),
(57, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-05-27 06:47:02', 0),
(58, 'felhasznalo', 'Langer Balázs belépett az oldalra', '2019-05-27 08:44:11', 0),
(59, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-05-27 09:20:31', 0),
(60, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-05-27 12:39:14', 0),
(61, 'felhasznalo', 'Langer Balázs belépett az oldalra', '2019-05-29 08:19:47', 0),
(62, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-05-29 08:26:41', 0),
(63, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-05-29 12:59:04', 0),
(64, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-05-29 20:40:43', 0),
(65, 'felhasznalo', 'Langer Balázs belépett az oldalra', '2019-06-03 07:37:45', 0),
(66, 'felhasznalo', 'Langer Balázs belépett az oldalra', '2019-06-03 09:49:41', 0),
(67, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-06-04 06:54:20', 0),
(68, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-06-11 06:47:58', 0),
(69, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-06-12 06:54:13', 0),
(70, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-06-24 10:56:26', 0),
(71, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-06-27 13:15:56', 0),
(72, 'felhasznalo', 'Ceglédi Iván az adminról kilépett.', '2019-06-27 13:16:32', 0),
(73, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-07-05 11:00:17', 0),
(74, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-07-08 10:07:38', 0),
(75, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-07-09 11:28:56', 0),
(76, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-07-10 09:01:45', 0),
(77, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-07-15 08:19:57', 0),
(78, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-07-18 10:07:22', 0),
(79, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-07-24 09:58:31', 0),
(80, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-07-25 07:57:32', 0),
(81, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-07-30 12:33:28', 0),
(82, 'felhasznalo', 'Ceglédi Iván kilépett.', '2019-07-30 13:52:07', 0),
(83, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-07-30 13:53:12', 0),
(84, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-07-31 06:43:18', 0),
(85, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-08-01 12:30:43', 0),
(86, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-08-07 07:54:03', 0),
(87, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-08-08 05:40:58', 0),
(88, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-08-15 11:31:02', 0),
(89, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-08-21 07:06:34', 0),
(90, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-08-22 10:08:38', 0),
(91, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-08-23 13:43:22', 0),
(92, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-09-02 12:33:28', 0),
(93, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-09-04 13:13:23', 0),
(94, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-09-06 13:22:22', 0),
(95, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-09-10 07:29:13', 0),
(96, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-09-11 07:51:42', 0),
(97, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-09-12 10:24:33', 0),
(98, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-09-13 09:30:41', 0),
(99, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-09-18 10:33:17', 0),
(100, 'felhasznalo', 'Ceglédi Iván az adminról kilépett.', '2019-09-18 10:37:19', 0),
(101, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-09-18 10:37:28', 0),
(102, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-09-18 14:51:56', 0),
(103, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-09-22 09:29:58', 0),
(104, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-09-23 16:10:29', 0),
(105, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-09-23 20:23:47', 0),
(106, 'felhasznalo', 'Ceglédi Iván kilépett.', '2019-09-23 20:30:34', 0),
(107, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-09-23 20:30:39', 0),
(108, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-09-24 06:35:29', 0),
(109, 'felhasznalo', 'Ceglédi Iván kilépett.', '2019-09-24 06:36:35', 0),
(110, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-09-24 06:36:48', 0),
(111, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-09-24 11:03:52', 0),
(112, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-09-26 10:03:04', 0),
(113, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-10-01 09:14:07', 0),
(114, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-10-01 13:13:37', 0),
(115, 'felhasznalo', 'Ceglédi Iván az adminról kilépett.', '2019-10-01 13:18:39', 0),
(116, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-10-01 13:18:46', 0),
(117, 'felhasznalo', 'Ceglédi Iván az adminról kilépett.', '2019-10-01 13:19:53', 0),
(118, 'felhasznalo', 'Besenyei Kálmán belépett az oldalra', '2019-10-01 13:20:10', 0),
(119, 'felhasznalo', 'Besenyei Kálmán az adminról kilépett.', '2019-10-01 13:21:16', 0),
(120, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-10-01 13:21:46', 0),
(121, 'felhasznalo', 'Ceglédi Iván az adminról kilépett.', '2019-10-01 13:22:16', 0),
(122, 'felhasznalo', 'Tóth László belépett az oldalra', '2019-10-01 13:22:40', 0),
(123, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-10-02 08:05:33', 0),
(124, 'felhasznalo', 'Besenyei Kálmán belépett az oldalra', '2019-10-02 12:05:20', 0),
(125, 'felhasznalo', 'Besenyei Kálmán kilépett.', '2019-10-02 12:05:50', 0),
(126, 'felhasznalo', 'Tóth László belépett az oldalra', '2019-10-02 12:05:53', 0),
(127, 'felhasznalo', 'Tóth László kilépett.', '2019-10-02 12:06:46', 0),
(128, 'felhasznalo', 'Tóth László belépett az oldalra', '2019-10-02 12:06:50', 0),
(129, 'felhasznalo', 'Besenyei Kálmán belépett az oldalra', '2019-10-02 12:07:02', 0),
(130, 'felhasznalo', 'Tóth László kilépett.', '2019-10-02 12:07:07', 0),
(131, 'felhasznalo', 'Tóth László belépett az oldalra', '2019-10-02 12:07:19', 0),
(132, 'felhasznalo', 'Besenyei Kálmán kilépett.', '2019-10-02 13:18:51', 0),
(133, 'felhasznalo', 'Vevő Egy belépett az oldalra', '2019-10-02 13:19:52', 0),
(134, 'felhasznalo', 'Vevő Egy kilépett.', '2019-10-02 13:21:20', 0),
(135, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-10-02 13:21:55', 0),
(136, 'felhasznalo', 'Ceglédi Iván kilépett.', '2019-10-02 13:23:02', 0),
(137, 'felhasznalo', 'Vevő Egy belépett az oldalra', '2019-10-02 13:23:12', 0),
(138, 'felhasznalo', 'Vevő Egy kilépett.', '2019-10-02 13:23:48', 0),
(139, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-10-02 13:24:14', 0),
(140, 'felhasznalo', 'Ceglédi Iván kilépett.', '2019-10-02 13:24:46', 0),
(141, 'felhasznalo', 'Vevő Ketto belépett az oldalra', '2019-10-02 13:25:44', 0),
(142, 'felhasznalo', 'Vevő Ketto az adminról kilépett.', '2019-10-02 13:26:11', 0),
(143, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-10-02 13:26:25', 0),
(144, 'felhasznalo', 'Ceglédi Iván kilépett.', '2019-10-02 13:26:45', 0),
(145, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-10-02 13:28:11', 0),
(146, 'felhasznalo', 'Besenyei Kálmán belépett az oldalra', '2019-10-02 17:48:30', 0),
(147, 'felhasznalo', 'Tóth László belépett az oldalra', '2019-10-03 09:15:00', 0),
(148, 'felhasznalo', 'Teszt User belépett az oldalra', '2019-10-03 09:23:16', 0),
(149, 'felhasznalo', 'Teszt User kilépett.', '2019-10-03 09:24:11', 0),
(150, 'felhasznalo', 'Teszt User belépett az oldalra', '2019-10-03 09:24:24', 0),
(151, 'felhasznalo', 'Tóth László kilépett.', '2019-10-03 10:03:52', 0),
(152, 'felhasznalo', 'Teszt User belépett az oldalra', '2019-10-03 10:04:51', 0),
(153, 'felhasznalo', 'Biró Marcell belépett az oldalra', '2019-10-07 08:33:15', 0),
(154, 'felhasznalo', 'Biró Marcell kilépett.', '2019-10-07 09:07:08', 0),
(155, 'felhasznalo', 'Biró Marcell belépett az oldalra', '2019-10-07 09:07:21', 0),
(156, 'felhasznalo', 'Biró Marcell kilépett.', '2019-10-07 09:10:47', 0),
(157, 'felhasznalo', 'Biró Marcell belépett az oldalra', '2019-10-07 09:10:55', 0),
(158, 'felhasznalo', 'Biró Marcell kilépett.', '2019-10-07 09:13:00', 0),
(159, 'felhasznalo', 'Biró Marcell belépett az oldalra', '2019-10-07 09:13:08', 0),
(160, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-10-07 09:29:36', 0),
(161, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-10-07 13:38:49', 0),
(162, 'felhasznalo', 'Teszt Ember belépett az oldalra', '2019-10-08 06:26:05', 0),
(163, 'felhasznalo', 'Biró Marcell belépett az oldalra', '2019-10-08 06:40:35', 0),
(164, 'felhasznalo', 'Tóth László belépett az oldalra', '2019-10-08 09:11:16', 0),
(165, 'felhasznalo', 'Tóth László az adminról kilépett.', '2019-10-08 12:46:42', 0),
(166, 'felhasznalo', 'Biró Marcell belépett az oldalra', '2019-10-08 14:31:12', 0),
(167, 'felhasznalo', 'Biró Marcell kilépett.', '2019-10-08 14:31:20', 0),
(168, 'felhasznalo', 'Teszt Ricsi belépett az oldalra', '2019-10-10 06:51:21', 0),
(169, 'felhasznalo', 'Teszt Ricsi kilépett.', '2019-10-10 06:53:27', 0),
(170, 'felhasznalo', 'Teszt Ricsi belépett az oldalra', '2019-10-10 06:53:38', 0),
(171, 'felhasznalo', 'Teszt Ricsi kilépett.', '2019-10-10 07:09:44', 0),
(172, 'felhasznalo', 'Teszt Ricsi belépett az oldalra', '2019-10-10 07:09:54', 0),
(173, 'felhasznalo', 'Tóth László belépett az oldalra', '2019-10-10 11:48:40', 0),
(174, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-10-11 12:34:50', 0),
(175, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-10-15 13:11:35', 0),
(176, 'felhasznalo', 'Ceglédi Iván az adminról kilépett.', '2019-10-15 13:12:50', 0),
(177, 'felhasznalo', '  kilépett.', '2019-10-15 13:13:08', 0),
(178, 'felhasznalo', 'Teszt Ricsi belépett az oldalra', '2019-10-17 09:21:54', 0),
(179, 'felhasznalo', 'Teszt Ricsi kilépett.', '2019-10-17 09:27:05', 0),
(180, 'felhasznalo', 'Teszt Ricsi belépett az oldalra', '2019-10-17 09:34:07', 0),
(181, 'felhasznalo', 'Teszt Ricsi kilépett.', '2019-10-17 11:07:25', 0),
(182, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-10-21 11:49:32', 0),
(183, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-10-22 05:52:30', 0),
(184, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-10-24 13:36:36', 0),
(185, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-10-25 08:53:39', 0),
(186, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-10-28 11:07:42', 0),
(187, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-10-28 14:20:59', 0),
(188, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-10-29 09:56:33', 0),
(189, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-10-29 15:10:17', 0),
(190, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-10-30 08:45:39', 0),
(191, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-11-15 08:57:09', 0),
(192, 'felhasznalo', 'Ceglédi Iván kilépett.', '2019-11-15 08:58:13', 0),
(193, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-11-15 08:59:17', 0),
(194, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-11-18 11:22:38', 0),
(195, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-11-19 10:33:31', 0),
(196, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2019-12-19 11:11:21', 0),
(197, 'felhasznalo', 'Ceglédi Iván belépett az oldalra', '2020-01-01 07:59:49', 0);

-- --------------------------------------------------------

--
-- Table structure for table `naplobejegyzesek`
--

CREATE TABLE `naplobejegyzesek` (
  `id` int(11) NOT NULL,
  `naploelemid` int(11) NOT NULL,
  `felhasznaloid` int(11) NOT NULL DEFAULT 0,
  `mentetid` int(11) NOT NULL DEFAULT 0,
  `uzenet` varchar(255) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `naplobejegyzesek`
--

INSERT INTO `naplobejegyzesek` (`id`, `naploelemid`, `felhasznaloid`, `mentetid`, `uzenet`) VALUES
(1, 1, 0, 0, ''),
(2, 2, 0, 0, ''),
(3, 1, 0, 0, ''),
(4, 1, 0, 0, ''),
(5, 1, 0, 0, ''),
(6, 3, 0, 5, ''),
(7, 1, 5, 0, ''),
(8, 4, 0, 0, ''),
(9, 5, 0, 0, ''),
(10, 1, 5, 0, ''),
(11, 1, 5, 0, ''),
(12, 1, 5, 0, ''),
(13, 1, 5, 0, ''),
(14, 1, 5, 0, ''),
(15, 1, 0, 0, ''),
(16, 1, 0, 0, ''),
(17, 1, 0, 0, ''),
(18, 1, 0, 0, ''),
(19, 1, 0, 0, ''),
(20, 1, 0, 0, ''),
(21, 1, 0, 0, ''),
(22, 1, 0, 0, ''),
(23, 1, 0, 0, ''),
(24, 1, 0, 0, ''),
(25, 1, 0, 0, ''),
(26, 1, 0, 0, ''),
(27, 1, 0, 0, ''),
(28, 6, 0, 0, ''),
(29, 6, 0, 0, ''),
(30, 7, 0, 10, ''),
(31, 2, 0, 0, ''),
(32, 7, 0, 10, ''),
(33, 3, 0, 5, ''),
(34, 8, 5, 0, ''),
(35, 4, 5, 0, ''),
(36, 5, 5, 0, ''),
(37, 9, 5, 0, ''),
(38, 8, 5, 0, ''),
(39, 8, 5, 0, ''),
(40, 7, 5, 10, ''),
(41, 8, 5, 0, ''),
(42, 10, 5, 0, ''),
(43, 10, 5, 0, ''),
(44, 10, 5, 0, ''),
(45, 8, 5, 0, ''),
(46, 8, 5, 0, ''),
(47, 7, 5, 10, ''),
(48, 8, 5, 0, ''),
(49, 11, 5, 15, 'B vitamin - J7S-2568'),
(50, 8, 5, 0, ''),
(51, 8, 5, 0, ''),
(52, 7, 5, 10, ''),
(53, 8, 5, 0, ''),
(54, 8, 5, 0, ''),
(55, 8, 5, 0, ''),
(56, 6, 5, 0, ''),
(57, 8, 5, 0, ''),
(58, 6, 5, 0, ''),
(59, 8, 5, 0, ''),
(60, 8, 5, 0, ''),
(61, 7, 5, 10, ''),
(62, 8, 5, 0, ''),
(63, 7, 5, 5, 'Teszt cikk'),
(64, 8, 5, 0, ''),
(65, 8, 5, 0, ''),
(66, 7, 5, 10, ''),
(67, 8, 5, 0, ''),
(68, 7, 5, 2, 'Új cuccok a webshopban'),
(69, 8, 5, 0, ''),
(70, 8, 5, 0, ''),
(71, 7, 5, 10, ''),
(72, 8, 5, 0, ''),
(73, 12, 5, 1, 'Akciós termékek'),
(74, 8, 5, 0, ''),
(75, 8, 5, 0, ''),
(76, 7, 5, 10, ''),
(77, 8, 5, 0, ''),
(78, 11, 5, 51, 'FiiO X7 Mark II - 15N-6694'),
(79, 8, 5, 0, ''),
(80, 8, 5, 0, ''),
(81, 7, 5, 10, ''),
(82, 8, 5, 0, ''),
(83, 8, 5, 0, ''),
(84, 8, 5, 0, ''),
(85, 13, 5, 0, ''),
(86, 8, 5, 0, ''),
(87, 8, 5, 0, ''),
(88, 7, 5, 10, ''),
(89, 8, 5, 0, ''),
(90, 13, 5, 0, ''),
(91, 8, 5, 0, ''),
(92, 6, 0, 0, ''),
(93, 6, 0, 0, ''),
(94, 7, 0, 10, ''),
(95, 11, 0, 16, 'CC vitamin - LGA-0615'),
(96, 7, 0, 10, ''),
(97, 10, 0, 0, ''),
(98, 10, 0, 0, ''),
(99, 10, 0, 0, ''),
(100, 7, 0, 10, ''),
(101, 10, 0, 0, ''),
(102, 10, 0, 0, ''),
(103, 10, 0, 0, ''),
(104, 7, 0, 10, ''),
(105, 10, 0, 0, ''),
(106, 10, 0, 0, ''),
(107, 10, 0, 0, ''),
(108, 7, 0, 10, ''),
(109, 10, 0, 0, ''),
(110, 10, 0, 0, ''),
(111, 10, 0, 0, ''),
(112, 7, 0, 10, ''),
(113, 6, 0, 0, ''),
(114, 7, 0, 10, ''),
(115, 7, 0, 10, ''),
(116, 7, 0, 10, ''),
(117, 7, 0, 10, ''),
(118, 7, 0, 10, ''),
(119, 7, 0, 10, ''),
(120, 7, 0, 10, ''),
(121, 7, 0, 10, ''),
(122, 6, 0, 0, ''),
(123, 7, 0, 10, ''),
(124, 12, 0, 1, 'Akciós termékek'),
(125, 7, 0, 10, ''),
(126, 10, 0, 0, ''),
(127, 10, 0, 0, ''),
(128, 10, 0, 0, ''),
(129, 7, 0, 10, ''),
(130, 10, 0, 0, ''),
(131, 10, 0, 0, ''),
(132, 10, 0, 0, ''),
(133, 7, 0, 10, ''),
(134, 10, 0, 0, ''),
(135, 10, 0, 0, ''),
(136, 10, 0, 0, ''),
(137, 7, 0, 10, ''),
(138, 10, 0, 0, ''),
(139, 10, 0, 0, ''),
(140, 10, 0, 0, ''),
(141, 7, 0, 10, ''),
(142, 6, 0, 0, ''),
(143, 7, 0, 10, ''),
(144, 2, 0, 0, ''),
(145, 7, 0, 10, ''),
(146, 3, 0, 5, ''),
(147, 8, 5, 0, ''),
(148, 4, 5, 0, ''),
(149, 5, 5, 0, ''),
(150, 9, 5, 0, ''),
(151, 8, 5, 0, ''),
(152, 8, 5, 0, ''),
(153, 7, 5, 10, ''),
(154, 8, 5, 0, ''),
(155, 8, 5, 0, ''),
(156, 7, 5, 10, ''),
(157, 8, 5, 0, ''),
(158, 7, 5, 10, ''),
(159, 8, 5, 0, ''),
(160, 8, 5, 0, ''),
(161, 8, 5, 0, ''),
(162, 7, 5, 10, ''),
(163, 8, 5, 0, ''),
(164, 8, 5, 0, ''),
(165, 8, 5, 0, ''),
(166, 7, 5, 10, ''),
(167, 8, 5, 0, ''),
(168, 8, 5, 0, ''),
(169, 8, 5, 0, ''),
(170, 8, 5, 0, ''),
(171, 7, 5, 10, ''),
(172, 8, 5, 0, ''),
(173, 8, 5, 0, ''),
(174, 8, 5, 0, ''),
(175, 8, 5, 0, ''),
(176, 7, 5, 10, ''),
(177, 8, 5, 0, ''),
(178, 8, 5, 0, ''),
(179, 8, 5, 0, ''),
(180, 8, 5, 0, ''),
(181, 7, 5, 10, ''),
(182, 8, 5, 0, ''),
(183, 6, 5, 0, ''),
(184, 8, 5, 0, ''),
(185, 8, 5, 0, ''),
(186, 7, 5, 10, ''),
(187, 8, 5, 0, ''),
(188, 6, 5, 0, ''),
(189, 8, 5, 0, ''),
(190, 8, 5, 0, ''),
(191, 7, 5, 10, ''),
(192, 8, 5, 0, ''),
(193, 7, 5, 10, ''),
(194, 8, 5, 0, ''),
(195, 7, 5, 10, ''),
(196, 8, 5, 0, ''),
(197, 7, 5, 10, ''),
(198, 8, 5, 0, ''),
(199, 7, 5, 10, ''),
(200, 8, 5, 0, ''),
(201, 7, 5, 10, ''),
(202, 8, 5, 0, ''),
(203, 7, 5, 10, ''),
(204, 8, 5, 0, ''),
(205, 7, 5, 10, ''),
(206, 8, 5, 0, ''),
(207, 7, 5, 10, ''),
(208, 8, 5, 0, ''),
(209, 7, 5, 10, ''),
(210, 8, 5, 0, ''),
(211, 7, 5, 10, ''),
(212, 8, 5, 0, ''),
(213, 7, 5, 10, ''),
(214, 8, 5, 0, ''),
(215, 7, 5, 10, ''),
(216, 8, 5, 0, ''),
(217, 7, 5, 10, ''),
(218, 8, 5, 0, ''),
(219, 7, 5, 10, ''),
(220, 8, 5, 0, ''),
(221, 7, 5, 10, ''),
(222, 8, 5, 0, ''),
(223, 7, 5, 10, ''),
(224, 8, 5, 0, ''),
(225, 7, 5, 10, ''),
(226, 8, 5, 0, ''),
(227, 7, 5, 10, ''),
(228, 8, 5, 0, ''),
(229, 7, 5, 10, ''),
(230, 8, 5, 0, ''),
(231, 7, 5, 10, ''),
(232, 8, 5, 0, ''),
(233, 7, 5, 10, ''),
(234, 8, 5, 0, ''),
(235, 7, 5, 10, ''),
(236, 8, 5, 0, ''),
(237, 7, 5, 10, ''),
(238, 8, 5, 0, ''),
(239, 7, 5, 10, ''),
(240, 8, 5, 0, ''),
(241, 7, 5, 10, ''),
(242, 8, 5, 0, ''),
(243, 7, 5, 10, ''),
(244, 8, 5, 0, ''),
(245, 7, 5, 10, ''),
(246, 8, 5, 0, ''),
(247, 7, 5, 10, ''),
(248, 8, 5, 0, ''),
(249, 6, 5, 0, ''),
(250, 8, 5, 0, ''),
(251, 8, 5, 0, ''),
(252, 7, 5, 10, ''),
(253, 8, 5, 0, ''),
(254, 10, 5, 0, ''),
(255, 10, 5, 0, ''),
(256, 8, 5, 0, ''),
(257, 8, 5, 0, ''),
(258, 7, 5, 10, ''),
(259, 8, 5, 0, ''),
(260, 10, 5, 0, ''),
(261, 10, 5, 0, ''),
(262, 8, 5, 0, ''),
(263, 8, 5, 0, ''),
(264, 7, 5, 10, ''),
(265, 8, 5, 0, ''),
(266, 11, 5, 21, 'GOG vitamin - FHR-4076'),
(267, 8, 5, 0, ''),
(268, 8, 5, 0, ''),
(269, 7, 5, 10, ''),
(270, 8, 5, 0, ''),
(271, 8, 5, 0, ''),
(272, 8, 5, 0, ''),
(273, 13, 5, 0, ''),
(274, 8, 5, 0, ''),
(275, 8, 5, 0, ''),
(276, 7, 5, 10, ''),
(277, 8, 5, 0, ''),
(278, 8, 5, 0, ''),
(279, 8, 5, 0, ''),
(280, 8, 5, 0, ''),
(281, 8, 5, 0, ''),
(282, 8, 5, 0, ''),
(283, 8, 5, 0, ''),
(284, 8, 5, 0, ''),
(285, 8, 5, 0, ''),
(286, 14, 5, 14, ''),
(287, 8, 5, 0, ''),
(288, 7, 5, 6, ''),
(289, 8, 5, 0, ''),
(290, 8, 5, 0, ''),
(291, 7, 5, 10, ''),
(292, 8, 5, 0, ''),
(293, 6, 5, 0, ''),
(294, 8, 5, 0, ''),
(295, 8, 5, 0, ''),
(296, 7, 5, 10, ''),
(297, 7, 0, 10, ''),
(298, 8, 5, 0, ''),
(299, 7, 5, 10, ''),
(300, 8, 5, 0, ''),
(301, 7, 5, 10, ''),
(302, 8, 5, 0, ''),
(303, 7, 5, 10, ''),
(304, 8, 5, 0, ''),
(305, 7, 5, 10, ''),
(306, 8, 5, 0, ''),
(307, 7, 5, 10, ''),
(308, 8, 5, 0, ''),
(309, 7, 5, 10, ''),
(310, 8, 5, 0, ''),
(311, 7, 5, 10, ''),
(312, 8, 5, 0, ''),
(313, 7, 5, 10, ''),
(314, 8, 5, 0, ''),
(315, 7, 5, 10, ''),
(316, 8, 5, 0, ''),
(317, 7, 5, 10, ''),
(318, 8, 5, 0, ''),
(319, 7, 5, 10, ''),
(320, 8, 5, 0, ''),
(321, 7, 5, 10, ''),
(322, 8, 5, 0, ''),
(323, 7, 5, 10, ''),
(324, 8, 5, 0, ''),
(325, 7, 5, 10, ''),
(326, 8, 5, 0, ''),
(327, 7, 5, 10, ''),
(328, 7, 0, 10, ''),
(329, 7, 0, 10, ''),
(330, 7, 0, 10, ''),
(331, 7, 0, 10, ''),
(332, 7, 0, 10, ''),
(333, 7, 0, 10, ''),
(334, 6, 0, 0, ''),
(335, 6, 0, 0, ''),
(336, 7, 0, 10, ''),
(337, 6, 0, 0, ''),
(338, 7, 0, 10, ''),
(339, 6, 0, 0, ''),
(340, 7, 0, 10, ''),
(341, 6, 0, 0, ''),
(342, 6, 0, 0, ''),
(343, 6, 0, 0, ''),
(344, 6, 0, 0, ''),
(345, 6, 0, 0, ''),
(346, 6, 0, 0, ''),
(347, 6, 0, 0, ''),
(348, 6, 0, 0, ''),
(349, 7, 0, 10, ''),
(350, 11, 0, 16, 'CC vitamin - LGA-0615'),
(351, 7, 0, 10, ''),
(352, 11, 0, 24, 'KK vitamin - SLT-0404'),
(353, 7, 0, 10, ''),
(354, 13, 0, 0, ''),
(355, 7, 0, 10, ''),
(356, 14, 0, 15, ''),
(357, 7, 0, 6, ''),
(358, 7, 0, 10, ''),
(359, 6, 0, 0, ''),
(360, 6, 0, 0, ''),
(361, 6, 0, 0, ''),
(362, 6, 0, 0, ''),
(363, 6, 0, 0, ''),
(364, 6, 0, 0, ''),
(365, 7, 0, 10, ''),
(366, 11, 0, 14, 'A vitamin - L98-1899'),
(367, 7, 0, 10, ''),
(368, 11, 0, 14, 'A vitamin - L98-1899'),
(369, 7, 0, 10, ''),
(370, 6, 0, 0, ''),
(371, 7, 0, 10, ''),
(372, 11, 0, 51, 'FiiO X7 Mark II - 15N-6694'),
(373, 7, 0, 10, ''),
(374, 6, 0, 0, ''),
(375, 7, 0, 10, ''),
(376, 11, 0, 16, 'CC vitamin - LGA-0615'),
(377, 7, 0, 10, ''),
(378, 11, 0, 16, 'CC vitamin - LGA-0615'),
(379, 7, 0, 10, ''),
(380, 6, 0, 0, ''),
(381, 6, 0, 0, ''),
(382, 7, 0, 10, ''),
(383, 6, 0, 0, ''),
(384, 6, 0, 0, ''),
(385, 7, 0, 10, ''),
(386, 2, 0, 0, ''),
(387, 7, 0, 10, ''),
(388, 3, 0, 5, ''),
(389, 8, 5, 0, ''),
(390, 4, 5, 0, ''),
(391, 5, 5, 0, ''),
(392, 9, 5, 0, ''),
(393, 8, 5, 0, ''),
(394, 8, 5, 0, ''),
(395, 4, 5, 0, ''),
(396, 5, 5, 0, ''),
(397, 9, 5, 0, ''),
(398, 8, 5, 0, ''),
(399, 8, 5, 0, ''),
(400, 7, 5, 10, ''),
(401, 8, 5, 0, ''),
(402, 8, 5, 0, ''),
(403, 7, 5, 10, ''),
(404, 8, 5, 0, ''),
(405, 7, 5, 10, ''),
(406, 8, 5, 0, ''),
(407, 8, 5, 0, ''),
(408, 7, 5, 10, ''),
(409, 8, 5, 0, ''),
(410, 8, 5, 0, ''),
(411, 8, 5, 0, ''),
(412, 7, 5, 10, ''),
(413, 8, 5, 0, ''),
(414, 8, 5, 0, ''),
(415, 8, 5, 0, ''),
(416, 8, 5, 0, ''),
(417, 7, 5, 10, ''),
(418, 8, 5, 0, ''),
(419, 8, 5, 0, ''),
(420, 8, 5, 0, ''),
(421, 8, 5, 0, ''),
(422, 7, 5, 10, ''),
(423, 8, 5, 0, ''),
(424, 6, 5, 0, ''),
(425, 8, 5, 0, ''),
(426, 8, 5, 0, ''),
(427, 7, 5, 10, ''),
(428, 8, 5, 0, ''),
(429, 6, 0, 0, ''),
(430, 7, 0, 10, ''),
(431, 6, 0, 0, ''),
(432, 7, 0, 10, ''),
(433, 2, 0, 0, ''),
(434, 7, 0, 10, ''),
(435, 3, 0, 5, ''),
(436, 8, 5, 0, ''),
(437, 4, 5, 0, ''),
(438, 5, 5, 0, ''),
(439, 9, 5, 0, ''),
(440, 8, 5, 0, ''),
(441, 7, 5, 10, ''),
(442, 8, 5, 0, ''),
(443, 8, 5, 0, ''),
(444, 8, 5, 0, ''),
(445, 7, 5, 10, ''),
(446, 8, 5, 0, ''),
(447, 8, 5, 0, ''),
(448, 8, 5, 0, ''),
(449, 8, 5, 0, ''),
(450, 8, 5, 0, ''),
(451, 8, 5, 0, ''),
(452, 8, 5, 0, ''),
(453, 8, 5, 0, ''),
(454, 8, 5, 0, ''),
(455, 8, 5, 0, ''),
(456, 8, 5, 0, ''),
(457, 8, 5, 0, ''),
(458, 8, 5, 0, ''),
(459, 8, 5, 0, ''),
(460, 8, 5, 0, ''),
(461, 8, 5, 0, ''),
(462, 8, 5, 0, ''),
(463, 8, 5, 0, ''),
(464, 8, 5, 0, ''),
(465, 8, 5, 0, ''),
(466, 8, 5, 0, ''),
(467, 8, 5, 0, ''),
(468, 8, 5, 0, ''),
(469, 8, 5, 0, ''),
(470, 8, 5, 0, ''),
(471, 8, 5, 0, ''),
(472, 8, 5, 0, ''),
(473, 8, 5, 0, ''),
(474, 8, 5, 0, ''),
(475, 8, 5, 0, ''),
(476, 8, 5, 0, ''),
(477, 8, 5, 0, ''),
(478, 8, 5, 0, ''),
(479, 8, 5, 0, ''),
(480, 8, 5, 0, ''),
(481, 8, 5, 0, ''),
(482, 8, 5, 0, ''),
(483, 8, 5, 0, ''),
(484, 8, 5, 0, ''),
(485, 8, 5, 0, ''),
(486, 4, 5, 0, ''),
(487, 5, 5, 0, ''),
(488, 9, 5, 0, ''),
(489, 8, 5, 0, ''),
(490, 7, 5, 10, ''),
(491, 8, 5, 0, ''),
(492, 8, 5, 0, ''),
(493, 6, 5, 0, ''),
(494, 8, 5, 0, ''),
(495, 8, 5, 0, ''),
(496, 7, 5, 10, ''),
(497, 6, 5, 0, ''),
(498, 7, 5, 10, ''),
(499, 7, 5, 10, ''),
(500, 7, 5, 10, ''),
(501, 6, 5, 0, ''),
(502, 6, 5, 0, ''),
(503, 7, 5, 10, ''),
(504, 7, 5, 10, ''),
(505, 7, 5, 10, ''),
(506, 7, 5, 10, ''),
(507, 6, 5, 0, ''),
(508, 6, 5, 0, ''),
(509, 6, 5, 0, ''),
(510, 6, 5, 0, ''),
(511, 6, 5, 0, ''),
(512, 6, 5, 0, ''),
(513, 6, 5, 0, ''),
(514, 6, 5, 0, ''),
(515, 15, 5, 0, ''),
(516, 6, 5, 0, ''),
(517, 6, 5, 0, ''),
(518, 6, 5, 0, ''),
(519, 6, 5, 0, ''),
(520, 6, 5, 0, ''),
(521, 6, 5, 0, ''),
(522, 6, 5, 0, ''),
(523, 6, 5, 0, ''),
(524, 6, 5, 0, ''),
(525, 6, 5, 0, ''),
(526, 6, 5, 0, ''),
(527, 6, 5, 0, ''),
(528, 6, 5, 0, ''),
(529, 6, 5, 0, ''),
(530, 6, 5, 0, ''),
(531, 6, 5, 0, ''),
(532, 6, 5, 0, ''),
(533, 6, 5, 0, ''),
(534, 6, 5, 0, ''),
(535, 6, 5, 0, ''),
(536, 6, 5, 0, ''),
(537, 7, 5, 10, ''),
(538, 7, 5, 10, ''),
(539, 7, 5, 10, ''),
(540, 7, 5, 10, ''),
(541, 7, 5, 10, ''),
(542, 7, 5, 10, ''),
(543, 7, 5, 10, ''),
(544, 7, 5, 10, ''),
(545, 7, 5, 10, ''),
(546, 6, 5, 0, ''),
(547, 6, 5, 0, ''),
(548, 6, 5, 0, ''),
(549, 6, 5, 0, ''),
(550, 6, 5, 0, ''),
(551, 6, 5, 0, ''),
(552, 15, 5, 0, ''),
(553, 6, 5, 0, ''),
(554, 6, 5, 0, ''),
(555, 6, 5, 0, ''),
(556, 15, 5, 0, ''),
(557, 6, 5, 0, ''),
(558, 6, 5, 0, ''),
(559, 6, 5, 0, ''),
(560, 6, 5, 0, ''),
(561, 6, 5, 0, ''),
(562, 6, 5, 0, ''),
(563, 6, 5, 0, ''),
(564, 6, 5, 0, ''),
(565, 6, 5, 0, ''),
(566, 6, 5, 0, ''),
(567, 6, 5, 0, ''),
(568, 6, 5, 0, ''),
(569, 6, 5, 0, ''),
(570, 6, 5, 0, ''),
(571, 6, 5, 0, ''),
(572, 7, 5, 10, ''),
(573, 6, 5, 0, ''),
(574, 6, 5, 0, ''),
(575, 6, 5, 0, ''),
(576, 6, 5, 0, ''),
(577, 6, 5, 0, ''),
(578, 6, 5, 0, ''),
(579, 6, 5, 0, ''),
(580, 6, 5, 0, ''),
(581, 6, 5, 0, ''),
(582, 6, 5, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `naploelemek`
--

CREATE TABLE `naploelemek` (
  `id` int(11) NOT NULL,
  `nev` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `tabla` varchar(50) COLLATE utf8_hungarian_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `naploelemek`
--

INSERT INTO `naploelemek` (`id`, `nev`, `tabla`) VALUES
(1, 'Naplo indítása', ''),
(2, 'Belépés oldal', ''),
(3, 'Belépés az oldalra', 'felhasznalok'),
(4, 'Fiókom oldal megtekintése', ''),
(5, 'Fiókom oldal adatmódosítás', ''),
(6, 'Főoldal megtekintése', ''),
(7, 'Bejegyzés megtekintése', 'post'),
(8, 'Rendelés számla letöltése', ''),
(9, 'Rendelések megtekintése', ''),
(10, 'Terméklista megtekintése', ''),
(11, 'Termék megtekintése', 'termek'),
(12, 'Termékek megtekintése cimke szerint', 'termek_cimkek'),
(13, 'Kosár oldal megjelenítése', ''),
(14, 'Rendelés mentése', 'rendelesek'),
(15, 'Hírlevél feliratkozás', '');

-- --------------------------------------------------------

--
-- Table structure for table `oldalak`
--

CREATE TABLE `oldalak` (
  `id` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `moduleleres` varchar(255) COLLATE utf8_hungarian_ci NOT NULL COMMENT 'modul/osztaly/metodus',
  `sorrend` int(11) NOT NULL,
  `parameter` text COLLATE utf8_hungarian_ci NOT NULL,
  `meta_id` int(11) NOT NULL DEFAULT 0,
  `torolheto` tinyint(4) NOT NULL DEFAULT 1,
  `elemnev` varchar(100) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `oldalak`
--

INSERT INTO `oldalak` (`id`, `url`, `moduleleres`, `sorrend`, `parameter`, `meta_id`, `torolheto`, `elemnev`) VALUES
(5, 'rolunk', 'post/postmegjelenites', 0, 'a:1:{s:7:\"post_id\";i:13;}', 0, 1, 'Rólunk - bejegyzés'),
(9, 'regisztracio', 'felhasznalok/felhasznalok/regisztracio', 10, '', 0, 0, ''),
(10, 'fiokom', 'felhasznalok/felhasznalok/fiokom', 0, '', 0, 0, ''),
(11, 'belepes', 'felhasznalok/felhasznalok/belepes', 10, '', 0, 1, ''),
(22, 'login', 'felhasznalok/felhasznalok/adminlogin', 0, '', 0, 1, ''),
(43, '', 'frontendlapok/frontendlapok/fooldalitartalmak', 10, '', 0, 1, ''),
(45, 'adatkezeles', 'post/postmegjelenites', 0, '', 0, 1, ''),
(47, 'kapcsolat', 'post/postmegjelenites', 10, 'a:1:{s:7:\"post_id\";s:1:\"7\";}', 0, 1, 'Kapcsolat - bejegyzés'),
(48, 'kapcsolat', 'urlapok/megjelenites', 0, '', 0, 1, ''),
(49, 'fiokom', 'rendelesek/rendelesmegjelenites/lista', 10, '', 0, 1, ''),
(51, 'hirlevel-feliratkozas', 'felhasznalok/felhasznalok/hirlevelfeliratkozas', 0, '', 0, 1, ''),
(52, '***', 'post/postmegjelenites', 0, 'a:1:{s:7:\"post_id\";s:2:\"10\";}', 0, 1, ' - bejegyzés'),
(53, 'adatvedelem', 'post/postmegjelenites', 0, 'a:1:{s:7:\"post_id\";s:2:\"12\";}', 0, 1, 'Adatvédelem - bejegyzés'),
(54, 'cookieinfo', 'post/postmegjelenites', 0, 'a:1:{s:7:\"post_id\";s:2:\"12\";}', 0, 1, 'Cookie info - bejegyzés');

-- --------------------------------------------------------

--
-- Table structure for table `oldal_urlek`
--

CREATE TABLE `oldal_urlek` (
  `id` int(11) NOT NULL,
  `url` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `nev` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `seo_description` text COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `oldal_urlek`
--

INSERT INTO `oldal_urlek` (`id`, `url`, `nev`, `seo_title`, `seo_description`) VALUES
(1, '', 'Főldal', 'Főoldal seo', 'Főoldal leírás'),
(2, '***', 'Oldal nem található', 'Hopsz, nem talált!', ''),
(3, 'akcios_termekek', 'Akciós termékek', 'Akciós termékek', ''),
(4, 'belepes', 'Belépés oldal', 'Belépés a webárházba', ''),
(5, 'fiokom', 'Fiókom', '', ''),
(6, 'hirek', 'Hírek', 'Hírek és újdonságok', ''),
(7, 'kapcsolat', 'Kapcsolat', 'Kapcsolat', ''),
(8, 'kosar', 'Kosár oldal', '', ''),
(9, 'login', 'Admin belépőoldal', '', ''),
(10, 'regisztracio', 'Regisztráció', 'Regisztráció', ''),
(11, 'reszletes', 'Termék végoldal', 'Termékek', ''),
(12, 'rolunk', 'Rólunk oldal', '', ''),
(13, 'termekek', 'Termék listázó oldal', 'Termékeink', ''),
(14, 'rendelesbefejezes', 'Rendelés befejezés - köszönet', '', ''),
(15, 'uj_termekek', 'Új termékek', '', ''),
(16, 'adatkezeles', 'Adatkezelés', '', ''),
(17, 'aszf', 'ASZF', '', ''),
(18, 'hirlevel-feliratkozas', 'Hírlevél feliratkozás', '', ''),
(19, 'adatvedelem', 'Adatvédelem', '', ''),
(20, 'cookieinfo', 'Cookie info', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `cim` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `bevezeto` tinytext COLLATE utf8_hungarian_ci NOT NULL,
  `szoveg` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(2) COLLATE utf8_hungarian_ci NOT NULL DEFAULT 'hu',
  `fokep` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `datum` timestamp NOT NULL DEFAULT current_timestamp(),
  `seo_title` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `seo_description` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `felhasznalo_id` int(11) NOT NULL,
  `megrekintve` int(11) NOT NULL DEFAULT 0,
  `tetszik` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `cim`, `bevezeto`, `szoveg`, `nyelv`, `fokep`, `datum`, `seo_title`, `seo_description`, `felhasznalo_id`, `megrekintve`, `tetszik`) VALUES
(1, 'Üdvözöllek a webáruházban', '', '<p><i><b>Tekintsd meg folyamatosan bővülő termékkatalógusunket</b></i></p>\r\n<p><span style=\"font-size: 12px;\">Most már mindent látsz, amit kell.</span><i><b><br></b></i></p>', 'hu', 'assets/post/fokep_1.jpg', '2019-04-09 12:41:12', '', '', 0, 0, 0),
(2, 'Új cuccok a webshopban', 'Phasellus rhoncus rutrum vestibulum. Duis lobortis justo ac tortor ullamcorper dictum. Fusce ut leo enim. Donec varius cursus justo, nec placerat orci.', '<p>\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac enim varius, imperdiet diam sed, eleifend libero. Maecenas eleifend sapien sed posuere efficitur. Cras vel venenatis neque. Maecenas ultricies tempor nisl eu accumsan. Curabitur volutpat, felis eu sagittis volutpat, felis neque auctor nunc, quis rutrum nibh elit nec lectus. In hac habitasse platea dictumst. Praesent iaculis libero sem, quis aliquam odio consequat in. Cras tincidunt purus a dignissim scelerisque. Fusce egestas sed lorem vitae sagittis. Vivamus tempus rhoncus ipsum, id gravida dui pulvinar vel. Nulla eget iaculis quam. </p><p>Phasellus rhoncus rutrum vestibulum. Duis lobortis justo ac tortor ullamcorper dictum. Fusce ut leo enim. Donec varius cursus justo, nec placerat orci.Pellentesque consequat elementum purus, at tincidunt dolor elementum a. Ut blandit quam nisi. Phasellus quis scelerisque eros. Duis facilisis dui purus. Aenean eget cursus dolor. Curabitur iaculis purus vitae egestas viverra. Nulla sodales ante vitae turpis feugiat finibus. Praesent gravida eros sed urna rutrum maximus. Nam eu auctor enim, sed sagittis felis. Fusce tellus nisi, congue nec gravida ut, tempus nec enim. Suspendisse laoreet purus sed lorem dapibus, at porttitor justo feugiat. Mauris id tortor elit.\r\n\r\n</p>', 'hu', 'assets/post/fokep_2.jpg', '2019-04-09 13:17:54', '', '', 0, 0, 0),
(4, 'Legújabb fejlesztések', 'Nunc convallis iaculis ex vitae laoreet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam quis ullamcorper elit, a semper risus...', '<p>\r\n\r\nNunc convallis iaculis ex vitae laoreet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam quis ullamcorper elit, a semper risus. Pellentesque ligula odio, finibus et sodales eu, cursus id tortor. Donec tempus sem ipsum, vitae facilisis massa facilisis eget.</p><p> Curabitur ut ante tincidunt, faucibus massa non, sagittis risus. Vestibulum vel enim eleifend, scelerisque nunc at, rhoncus arcu. Integer semper arcu et dapibus tincidunt. Sed augue odio, dignissim sed pharetra eget, porta non sem.Ut quis ligula faucibus, convallis ligula a, facilisis sem. </p><p>Mauris vitae quam et purus aliquet vestibulum. Donec mollis, orci ut condimentum dapibus, felis lectus semper neque, sed rutrum magna orci quis purus. Maecenas efficitur orci a ex ornare efficitur. In eu sapien sem. Proin nunc nulla, placerat ac gravida sed, malesuada vel dui. Pellentesque volutpat tortor ac enim fringilla, ut accumsan metus suscipit. Aliquam erat volutpat.\r\n\r\n</p>', 'hu', 'assets/post/fokep_4.jpg', '2019-04-09 13:22:48', '', '', 0, 0, 0),
(5, 'Teszt cikk', 'Donec malesuada venenatis purus, sed lobortis metus accumsan in. Nulla turpis enim, convallis nec elit ac, rhoncus auctor tellu...', '<p>\r\n\r\nAenean sapien arcu, pretium eu porttitor vestibulum, malesuada a lorem. Quisque porttitor ante vel finibus facilisis. Praesent quis egestas eros. Quisque pellentesque risus ac nunc blandit, et malesuada enim feugiat. Curabitur ornare mi at turpis rhoncus, sit amet pellentesque turpis laoreet. Etiam fringilla elementum leo sit amet rhoncus. Aliquam molestie condimentum dolor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. </p><p>Nullam id tempor leo, a vulputate augue. Donec malesuada venenatis purus, sed lobortis metus accumsan in. Nulla turpis enim, convallis nec elit ac, rhoncus auctor tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec id odio sed sem scelerisque hendrerit sed sed ante. Suspendisse congue a erat sed mattis. Sed vehicula, risus congue maximus rhoncus, leo velit convallis quam, non aliquam ipsum tortor vel ipsum. Sed euismod sit amet turpis vitae luctus.\r\n\r\n</p>', 'hu', 'assets/post/fokep_5.jpg', '2019-04-09 13:23:56', '', '', 0, 0, 0),
(6, 'Köszönjük megrendelését', 'köszönő szöveg a bejegyzések adminnál módosítható', 'köszönő szöveg a bejegyzések adminnál módosítható', 'hu', '', '2019-07-09 11:30:00', '', '', 0, 0, 0),
(7, 'Kapcsolat', '', '<p>\r\n\r\n    <strong>Ügyfélszolgálat</strong>:&nbsp;00 1 201-868-5959</p>\r\n<p><strong>Székhely</strong>:&nbsp;Secaucus, New Jersey, Egyesült Államok</p>\r\n<p><strong>Részvényárfolyam</strong>:&nbsp;VSI&nbsp;(NYSE)&nbsp;3,89&nbsp;USD&nbsp;+0,16 (+4,29%)\r\n\r\n</p>\r\n<p></p>\r\n<iframe style=\"box-shadow: 2px 2px 4px rgba(0,0,0,0.4);\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4949.778638223987!2d-122.80769106519645!3d50.32161850378425!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x548735a2463fa035%3A0x44f9607eb933d3b2!2sPemberton%2C+Brit+Kolumbia%2C+Kanada!5e0!3m2!1shu!2shu!4v1565164250333!5m2!1shu!2shu\" width=\"100%\" height=\"300\" frameborder=\"0\" allowfullscreen=\"\"></iframe>', 'hu', '', '2019-07-10 10:24:49', '', '', 0, 0, 0),
(8, '', '', '', 'hu', '', '2019-09-12 10:52:31', '', '', 0, 0, 0),
(9, 'Próba', 'Próba', 'Próba', 'hu', '', '2019-09-26 10:03:51', '', '', 0, 0, 0),
(10, '', 'teszt', '<h1 style=\"text-align: center;\"><span style=\"color: rgb(153, 153, 153); background-color: rgb(255, 255, 255);\">Oldal nem található</span></h1><p></p>', 'hu', '', '2019-09-26 10:23:43', '', '', 0, 0, 0),
(11, 'ASZF', '', '\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu libero augue. Proin aliquet lectus in egestas mollis. Aenean sed mi nec odio dignissim eleifend. Donec placerat libero at orci pellentesque viverra nec vel nisi. Maecenas fringilla purus quis est efficitur commodo. Nullam laoreet ex sem, at venenatis tortor scelerisque dapibus. Vestibulum ultricies mollis metus in ornare. Proin in varius mi, feugiat semper elit. Pellentesque in eleifend nisl, finibus sollicitudin felis. Vivamus interdum ipsum ac quam mollis posuere. Ut mollis scelerisque sapien sed euismod.Suspendisse dignissim quam metus, eu accumsan mauris ultricies vel. Proin mattis in ligula a elementum. Morbi ullamcorper risus et faucibus ultricies. Ut eget eros vestibulum, pretium nulla eget, fermentum lectus. Donec aliquam metus eu leo pharetra cursus. Integer dapibus quam risus, et consequat orci rhoncus in. Nunc facilisis mollis sapien, sed auctor dui fringilla ac. Quisque maximus hendrerit fringilla. Nullam sit amet gravida dolor, ac interdum est. Phasellus facilisis augue venenatis, porttitor erat convallis, tristique nisi. Vestibulum quis urna eget orci hendrerit facilisis. Praesent interdum tincidunt augue, eget sagittis urna luctus in. Praesent fringilla ex laoreet magna cursus, quis dapibus erat sollicitudin. Suspendisse potenti. Quisque quis enim elit. Fusce commodo lorem velit, et lacinia dolor commodo vitae.\r\n\r\n', 'hu', '', '2019-10-07 13:40:30', '', '', 0, 0, 0),
(12, 'Adatvédelem', '', '\r\nSuspendisse dignissim quam metus, eu accumsan mauris ultricies vel. Proin mattis in ligula a elementum. Morbi ullamcorper risus et faucibus ultricies. Ut eget eros vestibulum, pretium nulla eget, fermentum lectus. Donec aliquam metus eu leo pharetra cursus. Integer dapibus quam risus, et consequat orci rhoncus in. Nunc facilisis mollis sapien, sed auctor dui fringilla ac. Quisque maximus hendrerit fringilla. Nullam sit amet gravida dolor, ac interdum est. Phasellus facilisis augue venenatis, porttitor erat convallis, tristique nisi. Vestibulum quis urna eget orci hendrerit facilisis. Praesent interdum tincidunt augue, eget sagittis urna luctus in. Praesent fringilla ex laoreet magna cursus, quis dapibus erat sollicitudin. Suspendisse potenti. Quisque quis enim elit. Fusce commodo lorem velit, et lacinia dolor commodo vitae.\r\n\r\n', 'hu', '', '2019-10-07 13:56:34', '', '', 0, 0, 0),
(13, 'Rólunk', '', '\r\n\r\nMorbi aliquam lobortis ex eget malesuada. Curabitur a nisi auctor, dictum sem nec, bibendum leo. Etiam fermentum lectus felis, sit amet semper sem sodales at. In nibh arcu, vestibulum vel efficitur eu, pellentesque sit amet leo. In ultricies nisl vitae odio tempor, sed ultricies erat auctor. Donec aliquet mi ultricies justo interdum, ullamcorper malesuada mauris fringilla. Vestibulum pellentesque vehicula varius. Praesent convallis neque sit amet purus porta fringilla. Etiam eget elementum eros, a aliquet odio. Nulla vel tellus vel dolor aliquam consectetur. Pellentesque in tellus blandit, condimentum velit ac, suscipit tortor.\r\n\r\n', 'hu', '', '2019-10-07 13:58:01', '', '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `postxkategoria`
--

CREATE TABLE `postxkategoria` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `kategoria_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `postxkategoria`
--

INSERT INTO `postxkategoria` (`id`, `post_id`, `kategoria_id`) VALUES
(1, 2, 2),
(3, 1, 1),
(5, 4, 2),
(6, 5, 2),
(7, 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `post_kategoriak`
--

CREATE TABLE `post_kategoriak` (
  `id` int(11) NOT NULL,
  `kategorianev` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(2) COLLATE utf8_hungarian_ci NOT NULL DEFAULT 'hu',
  `leiras` text COLLATE utf8_hungarian_ci NOT NULL,
  `slug` varchar(30) COLLATE utf8_hungarian_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `post_kategoriak`
--

INSERT INTO `post_kategoriak` (`id`, `kategorianev`, `nyelv`, `leiras`, `slug`) VALUES
(1, 'Rendszer', 'hu', '0', ''),
(2, 'Hírek', 'hu', '0', 'hirek');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `kulcs` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `ertek` text COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `kulcs`, `ertek`) VALUES
(1, 'FRONTENDTEMA', 'mirolszol'),
(2, 'nyelvek', 'hu'),
(3, 'ADMINTEMA', 'vezerlo'),
(4, 'aruhaznev', 'VitaminShop'),
(5, 'aruhazurl', ''),
(6, 'admin_ertesites_email_felado', 'Mirolszol.com'),
(7, 'admin_ertesites_email_cim', 'cegledi.ivan74@gmail.com'),
(8, 'aruhaz_alapdeviza', 'HUF'),
(9, 'aruhaz_armegjelenites_tizedesek', '2'),
(10, 'soe_fooldali_title', 'mirolszol.com'),
(11, 'soe_fooldali_description', 'Tökéletes minőségű Olasz cipőt keresel? Látogass el hozzánk és válogass kedvedre a legjobb olasz cipő márkákból, Alberto Guardiani-től egészen Versace-ig!'),
(12, 'oldal_karbantartas_tajekoztato', 'Oldalunk jelenleg nem elérhető. Kérjük látogasson vissza később.'),
(13, 'aruhaz_armegjelenites', 'n'),
(15, 'oldal_karbantartas', '0'),
(16, 'admin_ertesites_rendelesrol', '1'),
(17, 'admin_ertesites_velemenyrol', '0'),
(18, 'admin_ertesites_ertekelesrol', '0'),
(19, 'admin_belso_ertesites_rendelesrol', '0'),
(20, 'admin_belso_ertesites_velemenyrol', '0'),
(21, 'admin_belso_ertesites_ertekelesrol', '0'),
(22, 'aruhaz_deviza_jeloles_arutan', '1'),
(23, 'imgsize.smallboxed.x', '80'),
(24, 'imgsize.smallboxed.y', '80'),
(25, 'imgsize.small.x', '160'),
(26, 'imgsize.small.y', '120'),
(27, 'imgsize.medium.x', '360'),
(28, 'imgsize.medium.y', '250'),
(29, 'imgsize.big.x', '1200'),
(30, 'imgsize.big.y', '800'),
(31, 'imgsize.mediumboxed.x', '200'),
(32, 'imgsize.mediumboxed.y', '200'),
(33, 'rendelesazonosito.elotag', 'CPWS-'),
(34, 'termeknev.termekjellemzo_id', '1'),
(35, 'kepek.nincskepurl', 'https://image.shutterstock.com/image-vector/no-image-available-icon-template-260nw-1036735678.jpg'),
(36, 'hirlevel.teszt.emailcimek', 'cegledi.ivan74@gmail.com'),
(37, 'google-signin-client_id', 'nincs'),
(38, 'facebook-app-id', '423366531800222'),
(39, 'ADMINURL', 'webshopadmin'),
(40, 'post.oldal.url', 'hirek'),
(41, 'termekek.oldal.url', 'termekek'),
(42, 'kosar.oldal.url', 'kosar'),
(43, 'regisztralas.vasarlaskor', '1'),
(44, '0', ''),
(45, 'admin.lapozo.limit', '30'),
(46, 'termeklista.cimkeszerint.limitdb', '9'),
(47, 'termek_valtozat_opcio_engedelyezes', '1'),
(48, 'szamlazz_user', ''),
(49, 'szamlazz_jelszo', ''),
(50, 'ceg_szamlaszam', ''),
(51, 'SMTP_host', 'sendmail@zente.org'),
(52, 'SMTP_user', 'sendmail@zente.org'),
(53, 'SMTP_password', 'send100MAIL'),
(54, 'Mail_sender_email', 'sendmail@zente.org'),
(55, 'kosar_betus_iranyitoszam', '0'),
(56, 'db_prefix', '');

-- --------------------------------------------------------

--
-- Table structure for table `sliderek`
--

CREATE TABLE `sliderek` (
  `id` int(11) NOT NULL,
  `nev` varchar(255) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `sliderek`
--

INSERT INTO `sliderek` (`id`, `nev`) VALUES
(1, 'Főoldali slider'),
(3, 'Szar2');

-- --------------------------------------------------------

--
-- Table structure for table `slider_kepek`
--

CREATE TABLE `slider_kepek` (
  `id` int(11) NOT NULL,
  `slider_id` int(11) NOT NULL,
  `kep` varchar(200) COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `slider_kepek`
--

INSERT INTO `slider_kepek` (`id`, `slider_id`, `kep`, `leiras`, `sorrend`) VALUES
(8, 1, 'assets/slider/1_park_canada_garden_pond_victoria_butchart_nature_81124_1920x1080.jpg', 'Leírás 1', 10),
(16, 1, 'assets/slider/1_main-slider-1180x400.jpg', 'Leírás 2', 0);

-- --------------------------------------------------------

--
-- Table structure for table `temavaltozok`
--

CREATE TABLE `temavaltozok` (
  `id` int(11) NOT NULL,
  `tema` varchar(10) COLLATE utf8_hungarian_ci NOT NULL,
  `kulcs` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `ertek` text COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adminmenu`
--
ALTER TABLE `adminmenu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aruhazak`
--
ALTER TABLE `aruhazak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cikkek`
--
ALTER TABLE `cikkek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_sablonok`
--
ALTER TABLE `email_sablonok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `felhasznalok`
--
ALTER TABLE `felhasznalok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `felhasznalo_csoportok`
--
ALTER TABLE `felhasznalo_csoportok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frontendmenu`
--
ALTER TABLE `frontendmenu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galeriak`
--
ALTER TABLE `galeriak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galeria_kepek`
--
ALTER TABLE `galeria_kepek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hirlevelek`
--
ALTER TABLE `hirlevelek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hirlevel_feliratkozok`
--
ALTER TABLE `hirlevel_feliratkozok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hozzaferesek`
--
ALTER TABLE `hozzaferesek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hozzaferesxcsoport`
--
ALTER TABLE `hozzaferesxcsoport`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kepek`
--
ALTER TABLE `kepek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kerdeskategoriak`
--
ALTER TABLE `kerdeskategoriak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kinezetielemek`
--
ALTER TABLE `kinezetielemek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leiratkozasok`
--
ALTER TABLE `leiratkozasok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metaadatok`
--
ALTER TABLE `metaadatok`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kulcs_2` (`kulcs`);

--
-- Indexes for table `naplo`
--
ALTER TABLE `naplo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `naplobejegyzesek`
--
ALTER TABLE `naplobejegyzesek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `naploelemek`
--
ALTER TABLE `naploelemek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oldalak`
--
ALTER TABLE `oldalak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oldal_urlek`
--
ALTER TABLE `oldal_urlek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `postxkategoria`
--
ALTER TABLE `postxkategoria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_kategoriak`
--
ALTER TABLE `post_kategoriak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliderek`
--
ALTER TABLE `sliderek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider_kepek`
--
ALTER TABLE `slider_kepek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temavaltozok`
--
ALTER TABLE `temavaltozok`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adminmenu`
--
ALTER TABLE `adminmenu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `aruhazak`
--
ALTER TABLE `aruhazak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `cikkek`
--
ALTER TABLE `cikkek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_sablonok`
--
ALTER TABLE `email_sablonok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `felhasznalok`
--
ALTER TABLE `felhasznalok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `felhasznalo_csoportok`
--
ALTER TABLE `felhasznalo_csoportok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `frontendmenu`
--
ALTER TABLE `frontendmenu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `galeriak`
--
ALTER TABLE `galeriak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `galeria_kepek`
--
ALTER TABLE `galeria_kepek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `hirlevelek`
--
ALTER TABLE `hirlevelek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `hirlevel_feliratkozok`
--
ALTER TABLE `hirlevel_feliratkozok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `hozzaferesek`
--
ALTER TABLE `hozzaferesek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `hozzaferesxcsoport`
--
ALTER TABLE `hozzaferesxcsoport`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kepek`
--
ALTER TABLE `kepek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kerdeskategoriak`
--
ALTER TABLE `kerdeskategoriak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `kinezetielemek`
--
ALTER TABLE `kinezetielemek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `leiratkozasok`
--
ALTER TABLE `leiratkozasok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `metaadatok`
--
ALTER TABLE `metaadatok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `naplo`
--
ALTER TABLE `naplo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=198;

--
-- AUTO_INCREMENT for table `naplobejegyzesek`
--
ALTER TABLE `naplobejegyzesek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=583;

--
-- AUTO_INCREMENT for table `naploelemek`
--
ALTER TABLE `naploelemek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `oldalak`
--
ALTER TABLE `oldalak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `oldal_urlek`
--
ALTER TABLE `oldal_urlek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `postxkategoria`
--
ALTER TABLE `postxkategoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `post_kategoriak`
--
ALTER TABLE `post_kategoriak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `sliderek`
--
ALTER TABLE `sliderek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `slider_kepek`
--
ALTER TABLE `slider_kepek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `temavaltozok`
--
ALTER TABLE `temavaltozok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
